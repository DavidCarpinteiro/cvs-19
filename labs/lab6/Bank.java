/*
Starting point for exercise of Lab Session 6.
CVS course - Integrated Master in Computer Science and Engineering
@FCT UNL Luis Caires 2015
*/

/*@
predicate AccountP(unit a,BankAccount c; int n) = c != null &*& AccountInv(c,n,?m);

predicate BankInv(Bank b; int n) =
	b != null
	&*& b.store |-> ?a
	&*& b.nelems |-> n
	&*& b.capacity |-> ?m
	
	&*& m == a.length 
	&*& 0 <= n
	&*& n <= m
	
	&*& array_slice_deep(a, 0, n, AccountP, unit,_,_)
	&*& array_slice(a, n, m, ?r)
	&*& all_eq(r, null) == true
	
	;
@*/

class Bank {

  BankAccount store[];
  int nelems;
  int capacity;

  public Bank(int size)
  //@ requires 0 < size;
  //@ ensures BankInv(this, 0);
  {
    nelems = 0;
    capacity = size;
    store = new BankAccount[size];
  }

  int addnewAccount(int code)
  //@ requires BankInv(this, ?n) &*& 0 <= code &*& code <= 1000;
  //@ ensures result == 0 ? BankInv(this, n+1) : BankInv(this, n);
  {
   	BankAccount c = new BankAccount(code);
   	
   	if(nelems < capacity) {
  	  store[nelems] = c;
  	  nelems = nelems + 1;
  	  return 0;
  	}
  	
  	return -1;
  }

  int getbalance(int code)
  //@ requires BankInv(this, ?n) &*& 0 <= code &*& code <= 1000;
  //@ ensures BankInv(this, n);
  {
    int i = 0;

    //@ open BankInv(this, n);
    while (i < nelems)
    //@ invariant BankInv(this, n) &*& 0 <= i &*& i <= n;
    {
	BankAccount acc = store[i];
       if ( acc.getcode() == code) {
       	return acc.getbalance();
       }
       i = i + 1;
    }
    return -1;
  }

  int removeAccount(int code)
  {
    int i = 0;
    
    while (i < nelems)
    {
       if ( store[i].getcode() == code) {
       	   if (i<nelems-1) {
       	     store[i] == store[nelems-1];
       	   }
       	   nelems = nelems - 1;
       	   store[nelems] = null;
       	 return 0;
       }
       i = i + 1;
    }
    return -1;
  }
}
