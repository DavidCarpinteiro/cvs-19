import java.util.concurrent.*;
import java.util.concurrent.locks.*;


/*@ 
    predicate CCounterInv(CCounter c; int v,int m) = 
        c.N |-> v 
        &*& c.MAX |-> m 
        &*& v>=0 &*& v<=m;
@*/
class CCounter {
	final SCounter counter;
  int N;
  int MAX;
  ReentrantLock mon;

  CCounter(int max)
  //@ requires 0 <= max;
  //@ ensures CCounterInv(this,0,max);
  { N = 0 ; MAX = max; mon = new ReentrantLock();}

  void inc()
  //@ requires CCounterInv(this,?n,?m) &*& n < m;
  //@ ensures CCounterInv(this,n+1,m);
  { 
  	mon.lock();
  	N++; 
  	mon.unlock();  
  }

  void dec()
  //@ requires CCounterInv(this,?n,?m) &*& n > 0;
  //@ ensures CCounterInv(this,n-1,m);
  { 
  	mon.lock();
  	N--; 
  	mon.unlock();  
  }
  
  int get()
  //@ requires CCounterInv(this,?n,?m);
  //@ ensures CCounterInv(this,n,m) &*& 0<=result &*& result<=m;
  { 
  	int tmp; //copy on stack
  	mon.lock();
  	tmp = N; 
  	mon.unlock();
  	return tmp;  
  }

  public static void main(String[] args) 
  //@ requires true;
  //@ ensures true;
  {
       int MAX = 100;
       CCounter c = new CCounter(MAX);
       //@ assert CCounterInv(c,0,MAX);
       //giveaway(c); // potentially give other thread access to c
        if (c.get() < MAX) {
            //@ assert CCounterInv(c,?v,MAX) &*& v < MAX;
            c.inc(); // not safe any more as other thread may have acted
        }
  }
}