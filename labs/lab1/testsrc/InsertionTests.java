import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class InsertionTests {

    private MyIntegerList generatedSortedList(){
        final MyIntegerList mil = new MyIntegerList();

        mil.push(1);
        mil.push(2);
        mil.push(3);
        mil.push(4);
        mil.push(5);
        mil.push(7);
        mil.push(8);
        mil.push(9);
        mil.push(10);
        mil.push(11);

        return mil;
    }

    private MyIntegerList generateList(int[] values){
        final MyIntegerList mil = new MyIntegerList();

        for (int value : values)
            mil.push(value);

        return mil;
    }

    @Test
    public void testPush() {
        final MyIntegerList mil = new MyIntegerList();

        assertEquals(0, mil.size());
        assertEquals("[]", mil.toString());

        mil.push(1);
        assertEquals(1, mil.size());
        assertNotEquals("[]", mil.toString());
        assertEquals("[1]", mil.toString());

        mil.push(2);
        assertEquals(2, mil.size());
        assertEquals("[1,2]", mil.toString());

        mil.push(1);
        assertEquals(3, mil.size());
        assertEquals("[1,2,1]", mil.toString());
    }

    @Test
    public void indexOf() {
        final MyIntegerList mil = generateList(new int[] {1,2,3});

        assertEquals(0, mil.indexOf(0));
        assertEquals(0, mil.indexOf(1));
        assertEquals(1, mil.indexOf(2));
        assertEquals(2, mil.indexOf(3));
        assertEquals(3, mil.indexOf(4));
        assertEquals(3, mil.indexOf(5));
    }

    @Test
    public void insertAt() {
        final MyIntegerList mil = generateList(new int[] {1,2,3});
        mil.insertAt(0,4);
        assertEquals("[1,2,3,0,4]", mil.toString());

        mil.insertAt(0,0);
        assertEquals("[0,1,2,3]", mil.toString());

        mil.insertAt(7,1);
        assertEquals("[0,7,1,2,3]", mil.toString());

        mil.insertAt(7,5);
        assertEquals("[0,7,1,2,3,7]", mil.toString());



        mil.insertAt(7,7);
        assertEquals("[0,7,1,2,3,7,0,7]", mil.toString());
    }

    @Test
    public void testOrderedPush(){
        final MyIntegerList mil = generatedSortedList();

        assertEquals(10, mil.size());
        assertNotEquals("[]", mil.toString());
        assertEquals("[1,2,3,4,5,7,8,9,10,11]", mil.toString());

        mil.sortedInsertion(6);
        assertEquals(11, mil.size());
        assertNotEquals("[]", mil.toString());
        assertEquals("[1,2,3,4,5,6,7,8,9,10,11]", mil.toString());

        mil.sortedInsertion(0);
        assertEquals(12, mil.size());
        assertNotEquals("[]", mil.toString());
        assertEquals("[0,1,2,3,4,5,6,7,8,9,10,11]", mil.toString());

        mil.sortedInsertion(12);
        assertEquals(12, mil.size());
        assertNotEquals("[]", mil.toString());
        assertEquals("[0,1,2,3,4,5,6,7,8,9,10,11,12]", mil.toString());

        //0123447 + 5 -> 01234547

    }

}