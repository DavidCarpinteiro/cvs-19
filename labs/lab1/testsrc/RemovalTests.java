import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RemovalTests {

    private MyIntegerList generatedPopulatedList() {
        final MyIntegerList mil = new MyIntegerList();

        mil.push(1);
        mil.push(2);
        mil.push(1);
        mil.push(6);
        mil.push(6);
        mil.push(7);
        mil.push(2);
        mil.push(2);
        mil.push(0);
        mil.push(5);

        return mil;
    }

    @Test
    void testPop() {
        final MyIntegerList mil = generatedPopulatedList();
        assertEquals(10, mil.size());
        assertEquals("[1,2,1,6,6,7,2,2,0,5]", mil.toString());

        int elem = mil.pop();
        assertEquals(5, elem);
        assertEquals(9, mil.size());
        assertEquals("[1,2,1,6,6,7,2,2,0]", mil.toString());

        elem = mil.pop();
        assertEquals(0, elem);
        assertEquals(8, mil.size());
        assertEquals("[1,2,1,6,6,7,2,2]", mil.toString());

        elem = mil.pop();
        assertEquals(2, elem);
        assertEquals(7, mil.size());
        assertEquals("[1,2,1,6,6,7,2]", mil.toString());

        for (int i = 0; i < 6; i++)
            mil.pop();

        elem = mil.pop();
        assertEquals(1, elem);
        assertEquals(0, mil.size());
        assertEquals("[]", mil.toString());
    }

}