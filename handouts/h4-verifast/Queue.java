/*@ predicate QueueInv(Queue q; int n, int m) = 
          q.front |-> ?f
      &*& q.rear |-> ?r
      &*& q.numberOfElements |-> n
      &*& q.maxElements |-> m
      &*& q.elements |-> ?a
      &*& a.length == m
      &*& 0 <= n &*& n <= m
      &*& 0 <= f &*& f < m
      &*& 0 <= r &*& r < m
      &*& array_slice(a,0,m,?items);
@*/

// FIFO
class Queue {
	
  int front;
  int rear;
  int numberOfElements;
  int maxElements;
  int[] elements;

  Queue(int max) 
  //@ requires max > 0;
  //@ ensures QueueInv(this, _, _);
  {
    front = 0;
    rear = 0;
    numberOfElements = 0;
    maxElements = max;
    elements = new int[max];
  }

  void enqueue(int v) 
  //@ requires QueueInv(this, ?n, ?m) &*& n < m;
  //@ ensures QueueInv(this, n + 1, m);
  {    
    elements[rear] = v;
    numberOfElements++;

    if(++rear == maxElements) {
      rear = 0;
    }
  }

  int dequeue() 
  //@ requires QueueInv(this, ?n, ?m) &*& n > 0;
  //@ ensures QueueInv(this, n - 1, m);
  {  
    int tmp = elements[front];
    numberOfElements--;

    if(++front == maxElements) {
      front = 0;
    }
    
    return tmp;
  }
  
  boolean isFull()
  //@ requires QueueInv(this, ?n, ?m);
  //@ ensures QueueInv(this, n, m) &*& result == (n == m);
  {
    return numberOfElements == maxElements;
  }

  
  boolean isEmpty() 
  //@ requires QueueInv(this, ?n, ?m);
  //@ ensures QueueInv(this, n, m) &*& result == (n == 0);
  {
    return numberOfElements == 0;
  }
  
  public static void main(String[] args) 
  //@ requires true;
  //@ ensures true;
  {

     Queue q = new Queue(5);
     
     if(!q.isFull()) {
	     //@ open QueueInv(q, _, _);
	     q.enqueue(1);
     }
     
     if(!q.isFull()) {
	     //@ open QueueInv(q, _, _);
	     q.enqueue(1);
     }
 
     if(!q.isEmpty()) {
	     //@ open QueueInv(q, _, _);
	     q.dequeue();
     }    
     
     if(!q.isEmpty()) {
	     //@ open QueueInv(q, _, _);
	     q.dequeue();
     }   

  }

}