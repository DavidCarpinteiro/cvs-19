import java.util.concurrent.locks.*;

/*@

predicate QueueP(Queue q, int n, int m) = 
	        q.queue |-> ?s 
	    &*& s != null
	    &*& QueueInv(s, n, m);


predicate_ctor CQueueSharedState(CQueue q)() =
	        q.queue |-> ?s 
	    &*& s != null
	    &*& QueueInv(s, _, _);

predicate_ctor CQueueSharedStateNotEmpty(CQueue q)() =
	        q.queue |-> ?s 
	    &*& s != null
	    &*& QueueInv(s, ?n, _)
	    &*& n > 0;

predicate_ctor CQueueSharedStateNotFull(CQueue q)() =
	        q.queue |-> ?s 
	    &*& s != null
	    &*& QueueInv(s, ?v, ?m)
	    &*& n < m;

predicate CQueueInv(CQueue c) =
	    c.mon |-> ?l
	&*& c.nonEmpty |-> ?cz
	&*& c.nonFull |-> ?cm
	&*& l != null
	&*& cz != null
	&*& cm != null
	&*& lck(l, 1, CQueueSharedState(c))
	&*& cond(cz,CQueueSharedState(c),CQueueSharedStateNotEmpty(c))
	&*& cond(cm,CQueueSharedState(c),CQueueSharedStateNotFull(c));
	
@*/

class CQueue {

	Queue queue;
	ReentrantLock mon;
	Condition nonEmpty;
	Condition nonFull;
	
	CQueue(int max) 
	//@ requires 0 <= max;
	//@ ensures CQueueInv(this);
	{
		queue = new Queue(max);
		//@ close CQueueSharedState(this)();
		//@ close enter_lck(1,CQueueSharedState(this));
		mon = new ReentrantLock();

		//@ close set_cond(CQueueSharedState(this), CQueueSharedStateNotEmpty(this));
		nonEmpty = mon.newCondition();
		//@ close set_cond(CQueueSharedState(this), CQueueSharedStateNotFull(this));
		nonFull = mon.newCondition();
		//@ close CCounterInv(this);
	}
	
	void enqueue() 
	//@ requires CQueueInv(this);
	//@ ensures CQueueInv(this);
	{
		//@ open CCounterInv(this);
		mon.lock();
		//@ open CCounterSharedState(this)();
		if( counter.get() == counter.getMax()) {
		  //@ close CCounterSharedState(this)();
		  nonMax.await();
		  //@ open CCounterSharedStateNotMax(this)();
		}
		//@ open BCounterInv(counter,_,_);
		this.counter.inc();
		//@ close CCounterSharedStateNotZero(this)();		
		nonZero.signal();
		mon.unlock();
		//@ close CCounterInv(this);
	}

	void dequeue() 
	//@ requires CQueueInv(this);
	//@ ensures CQueueInv(this);	
	{
		//@ open CCounterInv(this);
		mon.lock();
		//@ open CCounterSharedState(this)();
		if( counter.get() == 0) {
		  //@ close CCounterSharedState(this)();
		  nonZero.await();
		  //@ open CCounterSharedStateNotZero(this)();
		}
		//@ open BCounterInv(counter,_,_);
		this.counter.dec();
		//@ close CCounterSharedStateNotMax(this)();
		nonMax.signal();
		mon.unlock();
		//@ close CCounterInv(this);
	}
	
  	boolean isFull()
  	//@ requires CQueueInv(this);
  	//@ ensures CQueueInv(this);
  	{
  		int n;
		//@ open CCounterInv(this);
		mon.lock();
		//@ open CCounterSharedState(this)();
  		n = counter.get();
  		//@ close CCounterSharedState(this)();
  		mon.unlock();
  		return n; 
		//@ close CCounterInv(this);  		
  	} 

  	boolean isEmpty()
  	//@ requires CQueueInv(this);
  	//@ ensures CQueueInv(this);
  	{
  		int m;
		//@ open CQueueInv(this);
		mon.lock();
		//@ open CCounterSharedState(this)();
  		m = counter.get();
  		//@ close CCounterSharedState(this)();
  		mon.unlock();
  		return m; 
		//@ close CCounterInv(this);
  	}
  	
  	public static void main(String[] args)
  	//@requires true;
  	//@ ensures true;
  	{
  		
  		int count = Integer.parseInt(args.lenght() == 1 ? args[0] : "50");
  		count = count <= 0 ? 1 : count;
  		
  		CQueue q = new CQueue(count / 2);
  		
  		for(int i = 0; i < count; i++) {
  			new Thread(new Producer(q, i)).start();
  		}
  		
  		 for(int i = 0; i < count; i++) {
  			new Thread(new Consumer(q, i)).start();
  		}
  		
  		
  	}
}
