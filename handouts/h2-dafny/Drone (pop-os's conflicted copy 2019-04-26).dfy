class Drone {

    var speed_max:int;
    var speed:int;
    var current_pos:(int,int,int);

    var radius:int, height:int;

    var waypoints:seq<(int, int, int)>;

    constructor(r:int , h:int) 
        requires r > 0 && h > 0
        ensures Landed()    
    {
        radius := r;
        height := h;

        speed := 0;
        speed_max := 10;

        current_pos := (0,0,0);

        waypoints := [];
    }

    function method RepInv() : bool
        reads `speed, `speed_max, `waypoints, `radius, `height
    {
        0 <= speed <= speed_max
        && forall w :: w in waypoints ==> InsideBounds(w.0,w.1,w.2)

    }
    function method pow(b:int,e:nat):int        
        decreases e  
    { 
        if e == 0 then 1 else b*pow(b,e-1) 
    }
    function method InsideBounds(x:int, y:int, z:int):bool
        reads `radius, `height
    {
        if 0 <= z <= height
            then (-radius <= x <= radius) && (-radius <= y <= radius)
        else if height < z <= height + radius
            then ( pow(x, 2) +  pow(y, 2) +  pow(z, 2) ) <= pow(radius, 2)
        else false
    }
    function method OnTarget():bool
        reads `waypoints, `current_pos
    {
       |waypoints| > 0
       && waypoints[0].0 == current_pos.0
       && waypoints[0].1 == current_pos.1
       && waypoints[0].2 == current_pos.2
    }
    function method HasTarget():bool
        reads `waypoints
    {
       |waypoints| > 0
    }
    function method IsOnHeight():bool
        reads `current_pos, `height
    {
       current_pos.2 == height
    }
        function method IsOnGround():bool
        reads `current_pos
    {
       current_pos.2 == 0
    }

    function method Landed():bool
        reads `current_pos, `speed, `waypoints, `speed_max, `height, `radius
    {
        RepInv() && current_pos.2 == 0 && speed == 0 && |waypoints| == 0
    }
    function method Moving():bool
        reads `speed, `speed_max, `waypoints, `height, `radius
    {
       RepInv() && speed == speed_max && |waypoints| == 1
    }
    function method Idle():bool
        reads `speed, `speed_max, `waypoints, `height, `radius
    {
       RepInv() && speed == 0 && |waypoints| == 0
    }
    function method Patroling():bool
        reads `speed, `speed_max, `waypoints, `height, `radius
    {
       RepInv() && speed == speed_max / 2 && |waypoints| > 0
    }

    method TakeOff()
        modifies `speed, `waypoints
        requires Landed() && InsideBounds(current_pos.0, current_pos.1, height);
        ensures Moving()
    {
        speed := speed_max;
        waypoints := [(current_pos.0, current_pos.1, height)];
    }

    method CompletTakeOff()  
        modifies `speed, `waypoints
        requires Moving() && IsOnHeight()
        ensures Idle()
    {
        speed := 0;
        waypoints := [];
    }

    method Move(x:int, y:int, z:int) 
        modifies `speed, `waypoints
        requires Idle() && InsideBounds(x, y, z)
        ensures Moving()
    {
        speed := speed_max;
        waypoints := waypoints + [(x, y, z)];
    }

    method CompletMove()
        modifies `speed, `waypoints
        requires Moving() && OnTarget()
        ensures Idle()
    {
        speed := 0;
        waypoints := [];
    }
    method Land() 
        modifies `speed, `waypoints
        requires Idle() && InsideBounds(current_pos.0, current_pos.1, height);
        ensures Moving()
    {
        speed := speed_max;
        waypoints := [(current_pos.0, current_pos.1, 0)];

    }
    method CompletLand()
        modifies `speed, `waypoints
        requires Moving() && IsOnGround()
        ensures Landed()
    {
        speed := 0;
        waypoints := [];
    }
    
    method AddWaypoint(x:int, y:int, z:int) 
        modifies `speed, `waypoints
        requires Idle() || Patroling()
        requires InsideBounds(x, y, z)
        ensures Patroling()
    {
        speed := speed_max / 2;
        waypoints := waypoints + [(x, y, z)];
    }

    method NextWaypoint()
        modifies `speed, `waypoints
        requires Patroling() && OnTarget()
        ensures Idle() || Patroling()
    {
        waypoints := waypoints[1..];

        if(|waypoints| == 0)
        {
            speed := 0;
        }
    }
    

    method ReadGPS(x:int, y:int, z:int) 
        modifies `current_pos
    {
        current_pos := (x, y, z);
        print("GPS Updated: (");
        print(x);
        print(", ");
        print(y);
        print(", ");
        print(z);
        print(")\n");
    }

    static method Main() {
        var d := new Drone(10, 10);
        print("Taking Off\n");
        d.TakeOff();
        d.ReadGPS(1,2,10);

        if(d.IsOnHeight())
        {
            d.CompletTakeOff();
            print("Drone Online\n");
        }
        if(d.Idle() && d.InsideBounds(1,2,3))
        {
            print("Moving to Pos 1\n");
            d.Move(1,2,3);
            d.ReadGPS(1,2,3);
        }
        if(d.OnTarget())
        {
            d.CompletMove();
            print("Drone on Pos 1\n");
        }

        if(d.Idle() && d.InsideBounds(2,3,4))
        {
            print("Adding Waypoint 1\n");
            d.AddWaypoint(2,3,4);
        }
        if(d.Patroling() && d.InsideBounds(3,4,5))
        {
            print("Adding Waypoint 2\n");
            d.AddWaypoint(3,4,5);
            assert(d.Patroling());
            d.ReadGPS(2,3,4);
        }
        if(d.OnTarget())
        {
            print("Got to Waypoint 1\n");
            d.NextWaypoint();
        }
        if(d.InsideBounds(5,5,5) && d.Patroling())
        {
            print("Adding Waypoint 3\n");
            d.AddWaypoint(5,5,5);
            d.ReadGPS(3,4,5);
        }
        if(d.OnTarget())
        {
            print("Got to Waypoint 2\n");
            d.NextWaypoint();
            d.ReadGPS(5,5,5);
        }
        if(d.OnTarget())
        {
            print("Got to Waypoint 3\n");
            d.NextWaypoint();
        }
        if(d.Idle())
        {
            print("Preparing to Land\n");
            d.Land();
            d.ReadGPS(1,0,0);
        }
        if(d.Moving() && d.IsOnGround())
        {
            print("Drone Offline 1\n");
            d.CompletLand();
        }
        
    }


}