class Drone {

	// Constants
    var speed_max:int, radius:int, height:int;
	
	// State Variables
    var speed:int;
    var pos:(int,int,int);
    var waypoints:seq<(int, int, int)>;

	// Initializes a Drone at position (0,0,0) and max speed of 10
    constructor(r:int , h:int) 
        requires r > 0 && h > 0
        ensures Landed()    
    {
        radius := r;
        height := h;

        speed := 0;
        speed_max := 10;

        pos := (0,0,0);

        waypoints := [];
    }

    function method RepInv() : bool
        reads `speed, `speed_max, `waypoints, `radius, `height
    {
        0 <= speed <= speed_max
        && forall w :: w in waypoints ==> InsideBounds(w.0,w.1,w.2)

    }
	
	// ---- State Functions Begin ----
    function method Landed():bool
        reads `pos, `speed, `waypoints, `speed_max, `radius, `height
    {
        RepInv() && speed == 0 && pos.2 == 0 && |waypoints| == 0
    }
	
    function method Moving():bool
        reads `speed, `speed_max, `waypoints, `radius, `height, `pos
    {
       RepInv() && speed == speed_max && |waypoints| == 1
    }
	
    function method Idle():bool
        reads `speed, `speed_max, `waypoints, `radius, `height, `pos
    {
       RepInv() && speed == 0 && pos.2 > 0 && |waypoints| == 0 
    }
	
    function method Patroling():bool
        reads `speed, `speed_max, `waypoints, `radius, `height
    {
       RepInv() && speed == speed_max / 2 && |waypoints| > 0
    }
	// ---- State Functions End ----
	
	
	// ---- State Modifying Methods Begin ----
    method TakeOff()
        modifies `speed, `waypoints
        requires Landed() && InsideBounds(pos.0, pos.1, height)
        ensures Moving()
    {
        speed := speed_max;
        waypoints := [(pos.0, pos.1, height)];
    }

    method CompletTakeOff()  
        modifies `speed, `waypoints
        requires Moving() && IsOnHeight()
        ensures Idle()
    {
        speed := 0;
        waypoints := [];
    }

    method Move(x:int, y:int, z:int) 
        modifies `speed, `waypoints
        requires Idle() && InsideBounds(x, y, z)
        ensures Moving()
    {
        speed := speed_max;
        waypoints := waypoints + [(x, y, z)];
    }

    method CompletMove()
        modifies `speed, `waypoints
        requires Moving() && OnTarget()
        ensures Idle()
    {
        speed := 0;
        waypoints := [];
    }
	
    method Land() 
        modifies `speed, `waypoints
        requires Idle() && InsideBounds(pos.0, pos.1, 0)
        ensures Moving() 
    {
        speed := speed_max;
        waypoints := [(pos.0, pos.1, 0)];
    }
	
    method CompletLand()
        modifies `speed, `waypoints
        requires Moving() && IsOnGround()
        ensures Landed() 
    {
        speed := 0;
        waypoints := [];
    }
    
    method AddWaypoint(x:int, y:int, z:int) 
        modifies `speed, `waypoints
        requires ( Idle() || Patroling() ) && InsideBounds(x, y, z)
        ensures Patroling()
    {
        speed := speed_max / 2;
        waypoints := waypoints + [(x, y, z)];
    }

    method NextWaypoint()
        modifies `speed, `waypoints
        requires Patroling() && OnTarget()
        ensures Idle() || Patroling()
    {
        waypoints := waypoints[1..];
        if(|waypoints| == 0) {speed := 0;}
    }
    // ---- State Modifying Methods End ----

    

    // Helper funcion to calculate powers
    function method POW(b:int,e:nat):int        
        decreases e  
    { 
        if e == 0 then 1 else b*POW(b,e-1) 
    }
	
	// Check if a 3D point is inside the bounds of a cilinder - semi sphere
	// The waypoints of the Drone are limited to this cilinder - semi sphere
    function method InsideBounds(x:int, y:int, z:int):bool
        reads `radius, `height
    {
        if 0 <= z <= height
            then (-radius <= x <= radius) && (-radius <= y <= radius)
        else if height < z <= height + radius
            then ( POW(x, 2) +  POW(y, 2) +  POW(z, 2) ) <= POW(radius, 2)
        else false
    }
	
	// Checks if the position of Drone matches the current target
    function method OnTarget():bool
        reads `waypoints, `pos
    {
       HasTarget()
       && waypoints[0].0 == pos.0
       && waypoints[0].1 == pos.1
       && waypoints[0].2 == pos.2
       && pos.2 > 0 // Needed to not cause conflicts with Land
    }
	
	// Checks if Drone has a target
    function method HasTarget():bool
        reads `waypoints
    {
       |waypoints| > 0
    }
	
	// Check if Drone is at operational height and has single target
    function method IsOnHeight():bool
        reads `pos, `height, `waypoints
    {
       pos.2 == height && |waypoints| == 1 && pos.2 > 0 //Not sure why this is needed, height is always > 0...
    }
	
	// Check if Drone is at groud level and has single target
    function method IsOnGround():bool
        reads `pos, `waypoints
    {
       pos.2 == 0 && |waypoints| == 1
    }

    // Simulates receiving a GPS signal
    method setGPS(x:int, y:int, z:int) 
        modifies `pos
    {
        pos := (x, y, z);
		
        print("GPS Updated: ("); 
	    print(x); print(", "); 
	    print(y); print(", ");
        print(z); print(")\n");
    }

    // Returns the current position of the Drone
        method ReadGPS() returns (pos:(int, int, int))
    {
        return pos;
    }

    static method Main() {
		/*
			Example usage of Drone Class
		*/
        var d := new Drone(10, 10);

        if(d.Moving() || d.Idle() || d.Patroling()){print("Should not Print 0\n");}

        if(d.InsideBounds(d.pos.0, d.pos.1, d.height)) 
        {
            print("Taking Off\n");
            d.TakeOff();
            d.setGPS(1,2,10);
        }

        if(d.Landed() || d.Idle() || d.Patroling()){print("Should not Print 1\n");}

        if(d.Moving() && d.IsOnHeight())
        {
            d.CompletTakeOff();
            print("Drone Online\n");
        }

        if(d.Landed() || d.Moving() || d.Patroling()){print("Should not Print 2\n");}

        if(d.Idle() && d.InsideBounds(1,2,3))
        {
            print("Moving to Pos 1\n");
            d.Move(1,2,3);
            d.setGPS(1,2,3);
        }

        if(d.Landed() || d.Idle() || d.Patroling()){print("Should not Print 3\n");}

        if(d.OnTarget())
        {
            d.CompletMove();
            print("Drone on Pos 1\n");
        }

        if(d.Landed() || d.Moving() || d.Patroling()){print("Should not Print 4\n");}

        if(d.Idle() && d.InsideBounds(2,3,4))
        {
            print("Adding Waypoint 1\n");
            d.AddWaypoint(2,3,4);
        }

        if(d.Landed() || d.Idle() || d.Moving()){print("Should not Print 5\n");}

        if(d.Patroling() && d.InsideBounds(3,4,5))
        {
            print("Adding Waypoint 2\n");
            d.AddWaypoint(3,4,5);
            d.setGPS(2,3,4);
        }

        if(d.Landed() || d.Idle() || d.Moving()){print("Should not Print 6\n");}

        if(d.OnTarget())
        {
            print("Got to Waypoint 1\n");
            d.NextWaypoint();
        }

        if(d.Landed() || d.Idle() || d.Moving()){print("Should not Print 7\n");}

        if(d.Patroling() && d.InsideBounds(5,5,5))
        {
            print("Adding Waypoint 3\n");
            d.AddWaypoint(5,5,5);
            d.setGPS(3,4,5);
        }

        if(d.Landed() || d.Idle() || d.Moving()){print("Should not Print 8\n");}

        if(d.OnTarget())
        {
            print("Got to Waypoint 2\n");
            d.NextWaypoint();
            if(d.InsideBounds(5,5,5))
            {
                d.setGPS(5,5,5);
            }
        }

        if(d.Landed() || d.Idle() || d.Moving()){print("Should not Print 9\n");}

        if(d.OnTarget())
        {
            print("Got to Waypoint 3\n");
            d.NextWaypoint();
        }

        if(d.Landed() || d.Moving() || d.Patroling()){print("Should not Print 10\n");}

        if(d.Idle())
        {
            print("Preparing to Land\n");
            if(d.InsideBounds(d.pos.0, d.pos.1, 0)) 
            {
                d.Land();
            }
            if(d.InsideBounds(1,0,0))
            {
                d.setGPS(1,0,0);
            }
        }

        if(d.Landed() || d.Idle() || d.Patroling()){print("Should not Print 11\n");}

        if(d.Moving() && d.IsOnGround())
        {
            print("Drone Offline\n");
            d.CompletLand();
        }

        if(d.Moving() || d.Idle() || d.Patroling()){print("Should not Print 12\n");}
        
    }


}