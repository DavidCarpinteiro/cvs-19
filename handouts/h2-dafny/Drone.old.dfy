class Drone {

    var speed_max:int;
    var speed:int;
    var x:int, y:int, z:int;

    var radius:int, height:int;

    var target:seq<int>;
    //var t : (int, int, int) .1
    var waypoints:seq<seq<int>>;

    constructor(r:int , h:int) 
        requires r > 0 && h > 0
        ensures InsideBounds(x, y, z) && RepInv() && Landed()
    {
        radius := r;
        height := h;

        speed := 0;
        speed_max := 10;

        x := 0;
        y := 0;
        z := 0;

        target := [];
        waypoints := [];
    }

    function method RepInv() : bool
        reads this
    {
        0 <= speed <= speed_max
        && InsideBounds(x, y, z)
        && ( |target| == 0 || |target| == 3)
        && ( |target| == 0 ==> speed == 0 )
        && ( |target| == 3 ==> InsideBounds(target[0], target[1], target[2]) )
        && ( |target| == 3 ==> speed == speed_max )
        && ( forall w :: w in waypoints ==> (|w| == 3 && InsideBounds(w[0], w[1], w[2])) )
        && |waypoints| > 0 ==> |target| == 0
    }
    function method InsideBounds(x:int, y:int, z:int):bool
        reads `radius, `height
    {
        if 0 <= z <= height
            then (-radius <= x <= radius) && (-radius <= y <= radius)
        else if height < z <= height + radius
            then (x*x + y*y + z*z) <= (radius * radius)
        else false
    }
    function method CheckTarget():bool
        reads `target , `radius, `height
    {
        (|target| == 3 && InsideBounds(target[0], target[1], target[2])) || |target| == 0
    }
    function method HasWaypoint():bool
    reads `waypoints
    {
        |waypoints| > 0 && |waypoints[|waypoints|-1]| == 3
    }

    function method Landed():bool
        reads `z, `speed, `target, `waypoints
    {
        z == 0 && speed == 0 && |target| == 0 && |waypoints| == 0
    }
    function method Moving():bool
        reads `z, `speed, `speed_max, `target, `waypoints
    {
        speed == speed_max && |target| == 3  && |waypoints| == 0
    }
    function method Idle():bool
        reads `z, `speed, `speed_max, `target, `waypoints
    {
        speed == 0 && |target| == 0 && |waypoints| == 0
    }
    function method Patroling():bool
        reads `speed, `speed_max, `target, `waypoints
    {
       |waypoints| > 0 && speed == speed_max / 2 && |target| == 0
    }

    method TakeOff()
        modifies `speed, `target
        requires RepInv() && Landed()
        //requires InsideBounds(x, y, height)
        ensures RepInv() && Moving()
    {
        speed := speed_max;
        target := [x, y, height];
    }            
    method CompletTakeOff()  
        modifies `speed, `target, `z
        requires RepInv() && Moving()
        //requires  target[2] == height 
        //requires InsideBounds(target[0], target[1], target[2])//x, y
        ensures RepInv() && Idle()
    {
        speed := 0;
        z := height;//target[2];
        target := [];       
    }
    method Move(x_t : int, y_t : int, z_t : int)
        modifies `speed, `target
        requires RepInv() && Idle()
        requires InsideBounds(x_t, y_t, z_t)
        ensures RepInv() && Moving()
    {
        speed := speed_max;
        target := [x_t, y_t, z_t];
    }
    method CompletMove()
        modifies `speed, `target, `x, `y, `z
        requires RepInv() && Moving()
        requires InsideBounds(target[0], target[1], target[2])
        ensures RepInv() && Idle()
    {
        speed := 0;
        //TODO
        x := target[0];
        y := target[1];
        z := target[2];
        target := [];
    }
    method Land() 
        modifies `speed, `target
        requires RepInv() && Idle()
        //requires InsideBounds(x, y, 0)
        ensures RepInv() && Moving()
    {
        speed := speed_max;
        target := [x, y, 0];
    }
    method CompletLand()
        modifies `speed, `z, `target
        requires RepInv() && Moving()
        //requires target[2] == 0 
        requires InsideBounds(target[0], target[1], target[2])
        ensures RepInv() && Landed()
    {
        speed := 0;
        z := 0;//target[2];
        target := [];
    }
    method AddWaypoint(x_t : int, y_t : int, z_t : int)
        modifies `speed, `waypoints
        requires RepInv() 
        requires |waypoints| == 0 <==> Idle() 
        requires |waypoints| > 0 <==> Patroling()
        requires InsideBounds(x_t, y_t, z_t)
        ensures RepInv() && Patroling()
    {
        speed := speed_max / 2;
        waypoints := waypoints + [[x_t, y_t, z_t]];
    }

    method NextWaypoint()
        modifies `speed, `waypoints, `x, `y, `z
        requires RepInv() && Patroling()
        requires |waypoints| > 0 && |waypoints[|waypoints|-1]| == 3
        //requires InsideBounds(waypoints[|waypoints|-1][0], waypoints[|waypoints|-1][1], waypoints[|waypoints|-1][2])
        ensures RepInv()
        ensures |waypoints| == 0 <==> Idle()
        ensures |waypoints| > 0 <==> Patroling()
    {
        x := waypoints[|waypoints|-1][0];
        y := waypoints[|waypoints|-1][1];
        z := waypoints[|waypoints|-1][2];

        waypoints := waypoints[..|waypoints|-1]; 

        if(|waypoints| == 0)
        {
            speed := 0;
        }
    }

    static method Main() {
        var d := new Drone(10, 10);
        
        d.TakeOff();
        if(d.CheckTarget()) 
        {
            d.CompletTakeOff();
            print("Take Off\n");
        }
        if(d.InsideBounds(1, -1, 5) && d.Idle())
        {
            d.Move(1, -1, 5);
            print("Move\n");
        }
        if(d.CheckTarget() && d.Moving()) 
        {
            d.CompletMove();
            print("Complete Move\n");
        }
        if(d.InsideBounds(1, -2, 7) && d.Idle())
        {
            d.AddWaypoint(1, -2, 7);
            print("Waypoint\n");
            if(d.InsideBounds(5, -2, 7))
            {
                d.AddWaypoint(5, -2, 7);
                print("Waypoint\n");
                if(d.CheckTarget() && d.Patroling())
                {
                    if(d.HasWaypoint())
                    {
                        d.NextWaypoint();
                        print("Next Waypoint\n");
                        if(d.HasWaypoint())
                        {
                            d.NextWaypoint();
                            print("Next Waypoint\n");
                        }
                    }
                }
            }
        }
        if(d.Idle()) {
            d.Land();
            print("Land\n");
        }
        if(d.CheckTarget() && d.Moving()) 
        {
            d.CompletLand();
            print("Complete Land\n");
        }
    }
}